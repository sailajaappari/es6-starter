const max = arr => {
  return arr.reduce((x, y) => (x > y ? x : y), 0);
};
max([1, 2, 3, 4, 5]);

// Unique

const findUnique = (res, val) => {
  if (res.indexOf(val) === -1) res.push(val);
  return res;
};

export const unique = arr => arr.reduce(findUnique, []);

unique([1, 2, 3, 1, 3, 5, 7]);

// toArray
export const toArray = (...args) =>
  args.reduce((res, ele) => {
    res.push(ele);
    return res;
  }, []);
toArray(1, 2, 3, 4);
