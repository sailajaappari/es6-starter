import { add, odd } from './map_filter_reduce';

export const compose = (...fns) => (...args) =>
  fns.reduce((f, g) => f(g(...args)));

compose(add, odd)(1, 2, 3, 4);

export const pipeline = (fn, ...fns) => (...args) =>
  fns.reduce((res, fun) => fun(res), fn(...args));
