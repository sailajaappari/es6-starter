import { filter, map, reduce } from "./map_filter_reduce";
import { max, toArrayp, unique } from "./map_filter_reduce";
import { compose } from "./map_filter_reduce";

export const increment = x => x + 1;

export const odd = x => x % 2 !== 0;

export const add = (x, y) => x + y;
