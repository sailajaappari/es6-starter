import { add, increment, odd } from './map_filter_reduce';

// Map Function
export const map = function(fun) {
  return function* elements(arr) {
    for (const val of arr) {
      yield fun(val);
    }
  };
};

map(increment)([10, 20, 30, 40]);

// Filter Function
export const filter = function(fun) {
  return function* elements(arr) {
    for (const val of arr) {
      if (fun(val)) yield val;
    }
  };
};

filter(odd)([1, 2, 3, 4, 5]);

// Reduce Function
export const reduce = function(fun, initialValue) {
  const res = initialValue;
  return function* elements(arr) {
    for (const val of arr) {
      res = yield fun(res, val);
    }
  };
};

reduce(add, 0)([1, 2, 3, 4, 5]);
